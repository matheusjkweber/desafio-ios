//
//  String+Extension.swift
//  DesafioUol
//
//  Created by matheus weber on 10/2/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit

extension Data {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options:[NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
        }
        return nil
    }
}
extension String {
    var data: Data {
        return Data(utf8)
    }
}

