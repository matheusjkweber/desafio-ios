//
//  DetailViewController.swift
//  DesafioUol
//
//  Created by matheus weber on 10/2/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit
import Social

class ShotDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shotImageView: UIImageView!
    
    @IBOutlet weak var viewsCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var selectLabel: UILabel!
    var shot:ShotModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if shot != nil{
            titleLabel.text = shot?.title
            viewsCount.text = "Views count: \(shot?.views_count ?? 0)"
            commentCount.text = "Comments count: \(shot?.comments_count ?? 0)"
            createdAt.text = "Created at \(shot?.getFormattedDate() ?? "") by \(shot?.author?.name ?? "")"
            
            if !(shot?.description.isEmpty)!{
                descriptionTitle.text = "Description"
                descriptionTextView.text = ""
                descriptionTextView.attributedText = shot?.description.data.attributedString
            }
            
            selectLabel.isHidden = true
            
            if let image = shot?.image{
                if !(image.teaser.isEmpty){
                    if let url = URL(string: image.teaser){
                        shotImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "defaultImage"))
                    }
                }
            }
            
            let facebookButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            facebookButton.setBackgroundImage(#imageLiteral(resourceName: "facebookIcon"), for: .normal)
            facebookButton.addTarget(self, action: #selector(didTapFacebookButton), for: .touchUpInside)
            
            let twitterButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            twitterButton.setBackgroundImage(#imageLiteral(resourceName: "twitterIcon"), for: .normal)
            twitterButton.addTarget(self, action: #selector(didTapTwitterButton), for: .touchUpInside)
            
            navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: facebookButton), UIBarButtonItem(customView: twitterButton)]
            
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func didTapFacebookButton(sender: Any){
        if let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook){
            vc.add(shotImageView.image)
            //vc.add(URL(string: "http://www.example.com/"))
            vc.setInitialText("I am sharing my shot with name \(shot?.title ?? "") from author \(shot?.author?.name ?? "")")
            self.present(vc, animated: true, completion: nil)
        }else{
            CommonMethods.showAlertMessage(self, titleStr: "Something went wrong!", messageStr: "Please try again.")
        }
        
    }
    
    func didTapTwitterButton(sender: Any){
        if let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter){
            vc.add(shotImageView.image)
            //vc.add(URL(string: "http://www.example.com/"))
            vc.setInitialText("I am sharing my shot with name \(shot?.title ?? "") from author \(shot?.author?.name ?? "")")
            self.present(vc, animated: true, completion: nil)
        }else{
            CommonMethods.showAlertMessage(self, titleStr: "Something went wrong!", messageStr: "Please try again.")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

