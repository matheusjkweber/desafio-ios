//
//  MasterViewController.swift
//  DesafioUol
//
//  Created by matheus weber on 10/2/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit
import SDWebImage

class ShotsListViewController: UITableViewController {

    var detailViewController: ShotDetailViewController? = nil
    let reuseIdentifier = "Cell"
    var shots = [ShotModel]()
    var page = 1
    var nextLoading = 30
    var limit = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? ShotDetailViewController
        }
        prepareTableView()
        populateShots()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareTableView(){
        tableView.register(UINib(nibName: "ShotCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.separatorColor = UIColor.black
        tableView.layoutMargins = UIEdgeInsets.zero
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    func refresh(){
        page = 1
        nextLoading = 30
        populateShots()
    }
    
    func stopRefresher(){
        refreshControl?.endRefreshing()
    }
    
    func populateShots(){
        if isReachable {
            CommonMethods.loaderShow()
            RouterService.sharedInstance.FetchShots(with: page, limit: limit, { (shotsList) in
                if self.page == 1{
                    self.shots = shotsList
                }else{
                    self.shots.append(contentsOf: shotsList)
                }
                self.tableView.reloadData()
                self.stopRefresher()
                CommonMethods.loaderHide()
            })
        } else {
            CommonMethods.showAlertMessage(self)
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! ShotDetailViewController
                controller.shot = shots[indexPath.row]
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                
                if view.traitCollection.userInterfaceIdiom == .pad && splitViewController?.displayMode == .primaryOverlay {
                    let animations: () -> Void = {
                        self.splitViewController?.preferredDisplayMode = .primaryHidden
                    }
                    let completion: (Bool) -> Void = { _ in
                        self.splitViewController?.preferredDisplayMode = .automatic
                    }
                    UIView.animate(withDuration: 0.3, animations: animations, completion: completion)
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shots.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: reuseIdentifier,
            for: indexPath) as? ShotCell{
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            cell.setup(shot: shots[indexPath.row])
            
            if let image = shots[indexPath.row].image{
                if !(image.teaser.isEmpty){
                    if let url = URL(string: image.teaser){
                        cell.shotImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "defaultImage"))
                    }
                }
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 87
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == nextLoading - 1{
            page = page + 1
            nextLoading = nextLoading + 30
            populateShots()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: nil)
        
    }
}

