//
//  ShotModel.swift
//  DesafioUol
//
//  Created by matheus weber on 8/19/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit

class ShotModel{
    class ImageModel{
        var normal:String = ""
        var teaser:String = ""
        
        init(with json: anyDict){
            self.normal = json["normal"] as? String ?? ""
            self.teaser = json["teaser"] as? String ?? ""
        }
    }
    
    class AuthorModel{
        var name:String = ""
        
        init(with json: anyDict){
            self.name = json["name"] as? String ?? ""
        }
    }
    
    var id:Int = 0
    var title:String = ""
    var description:String = ""
    var views_count:Int = 0
    var likes_count:Int = 0
    var comments_count:Int = 0
    var image: ImageModel?
    var created_at:String = ""
    var author: AuthorModel?
    
    init(with json: anyDict){
        self.id = json["id"] as? Int ?? 0
        self.title = json["title"] as? String ?? ""
        self.description = json["description"] as? String ?? ""
        self.views_count = json["views_count"] as? Int ?? 0
        self.likes_count = json["likes_count"] as? Int ?? 0
        self.comments_count = json["comments_count"] as? Int ?? 0
        self.created_at = json["created_at"] as? String ?? ""
        
        if let image = json["images"] as? anyDict{
            self.image = ImageModel(with: image)
        }
        
        if let author = json["user"] as? anyDict{
            self.author = AuthorModel(with: author)
        }
    }
    
    func getFormattedDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        if let date = dateFormatter.date(from: self.created_at){
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.string(from: date)
        }
        return ""
    }

    
}
