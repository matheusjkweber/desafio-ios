//
//  Router.swift
//  DesafioUol
//
//  Created by matheus weber on 8/19/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class RouterService {
    
    let sessionManager = SessionManager()
    
    static let sharedInstance = RouterService()
    
    enum Router: URLRequestConvertible {
        case getShots(page: Int, limit: Int)
        case getShot(id: Int)
        
        func asURLRequest() throws -> URLRequest {
            
            let (verb, path, parameters): (String, String, [String: Any]?) = {
                switch self {
                    case .getShots(let page, let limit):
                        return ("GET", "\(Endpoints.urlEndpoint(.Shots))?page=\(page)&per_page=\(limit)", nil)
                    case .getShot(let id):
                        return ("GET", "\(Endpoints.urlEndpoint(.Shots))/\(id)", nil)
                }
            }()
            
            var urlRequest = URLRequest(url: URL(string: path)!)
            
            urlRequest.httpMethod = verb
            
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("Bearer \(Endpoints.apiKey)", forHTTPHeaderField: "Authorization")

            urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
            urlRequest.timeoutInterval = 60.0

            if let parameters = parameters {
                CommonMethods.printJSON(dictionary: parameters)
            }
            
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
        }
    }
    
    func FetchShots(with page: Int, limit: Int, _ callback: @escaping (([ShotModel]) -> Void)) {
        requestWithJSON(Router.getShots(page: page, limit: limit), resultKey: "") { ( jsonData: [anyDict]?, error) in
            var shotList = [ShotModel]()
            if jsonData != nil {
                for shot in jsonData! {
                    shotList.append(ShotModel(with: shot))
                }
            }
            
            callback(shotList)
        }
    }
    
    func FetchShot(with id: Int, _ callback: @escaping ((ShotModel?) -> Void)) {
        requestWithJSON(Router.getShot(id: id), resultKey: "") { ( jsonData: anyDict?, error) in
            
            if jsonData != nil {
                let shot = ShotModel(with: jsonData!)
                callback(shot)
            
            }
            
            callback(nil)
        }
    }

    
    //MARK: Core
    
    func requestWithSuccess(_ request: URLRequestConvertible, callback: @escaping successComplition ) {
        requestWithSuccessJSON(request) { ( success, error) in
            //print(error)
            //print(success)
            
            return success != nil ? callback(true) : callback(false)
        }
    }
    
    func requestWithSuccessError(_ request: URLRequestConvertible, callback: @escaping successErrorComplition ) {
        requestWithSuccessJSON(request) { ( success, error) in
            if success != nil {
               // print(message)
                callback(true, nil)
            } else {
                //print(error)
                callback(false, error!)
            }
        }
    }
    
    func requestWithJSON<T>(_ requestURL:URLRequestConvertible, resultKey: String = "data",  callback: @escaping (T?, String? ) -> Void) {
        sessionManager.request(requestURL)
            .validate()
            .responseJSON() { response in
                //print(requestURL.urlRequest)
                //print(response)
                //print(response.result)
                //print(response.result.value)
                switch response.result {
                    
                case .success (_):
                    if !resultKey.isEmpty{
                        guard let validJSON = response.result.value as? [String: Any], let validData = validJSON[resultKey] as? T else {
                            
                            print("Error while fetching")
                            callback(nil, "Not Found Data")
                            return
                        }
                        callback(validData, nil)
                    }else{
                        if let validJSON = response.result.value as? T{
                            callback(validJSON, nil)
                        }
                    }
                    
                    
                case .failure(_):
                    do {
                        if let parsedData = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any], let error = parsedData["error"] as? String  {
                            callback(nil, error)
                        } else {
                            callback(nil, "Error while fetching data")
                        }
                        
                    } catch {
                        callback(nil, "Error while connecting to Dribbble. Please try again")
                    }
                    break
                }
        }
    }
    
    func requestWithSuccessJSON(_ requestURL:URLRequestConvertible, callback: @escaping (String?, String?) -> Void) {
        sessionManager.request(requestURL)
            .validate()
            .responseJSON() { response in
                
                switch response.result {
                    
                case .success (_):
                    guard let validJSON = response.result.value as? [String: Any], let success = validJSON["success"] as? String  else {
                        callback(nil,  "Error while parsing data")
                        return
                    }
                    callback(success,  nil)
                case .failure(_):
                    
                    do {
                        if let parsedData = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any], let error = parsedData["error"] as? String  {
                            callback(nil, error)
                        } else {
                            callback(nil, "Error while fetching data")
                        }
                        
                    } catch {
                        callback(nil, "Error while connecting to Dribbble. Please try again")
                    }
                    
                    break
                }
        }
    }
    
}
