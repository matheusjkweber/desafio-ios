//
//  Endpoints.swift
//  DesafioUol
//
//  Created by matheus weber on 8/12/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import Foundation

struct Endpoints {
    
    enum URI : String {
    
        case Shots = "/shots"
        
    }

    static let baseImageURL = ""
    
    fileprivate static let baseURL = "https://api.dribbble.com/v1"
    static let apiKey = "cbcbe36666dd6d0f2aff5106c9d98e93c23de2aeab7d0f6708e26bea7d1ed7bb"

    
    static func urlEndpoint(_ uri:URI, id:Int? = nil) -> String {
        var url = "\(baseURL)\(uri.rawValue)"
        if let id = id {
            url += "/\(id)"
        }
        return url
    }
}
