//
//  CommonMethods.swift
//  PunkBeer
//
//  Created by matheus weber on 8/12/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit
import SVProgressHUD

typealias anyDict = [String: Any]
typealias successErrorComplition = (Bool, String?) -> Void
typealias successComplition = (Bool) -> Void


struct CommonMethods {
    
    static let getScreenSize = UIScreen.main.bounds
    
    static func showAlertMessage(_ vc: UIViewController, titleStr:String = "Oops! Something went wrong.", messageStr:String = "Please check your internet connection and try again.") -> Void {
        
        let alert = UIAlertController(title:titleStr, message:messageStr, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default)
        { (reuslt) in
            
        }
        alert.addAction(action)
        
        vc.present(alert, animated: true){}
    }
    
    static func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
   static func getTimestamp() -> Int{
        
        let timezone = NSTimeZone.local.secondsFromGMT()
        
        var date = Date()
        date = date.addingTimeInterval(TimeInterval( -timezone ))
        
        return Int((date.timeIntervalSince1970 * 1000.0).rounded())
        
    }
    
    static func convertTimestampToLocal(timestamp: Int) -> Int{
    
        var timezone = NSTimeZone.local.secondsFromGMT()
        timezone = timezone * 1000
        
        return timestamp + timezone
    }
    
    static func convertDate(timestamp: Int) -> Date{
        var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
        let finalDate = Date(timeIntervalSince1970: TimeInterval(timestamp)).addingTimeInterval(TimeInterval(secondsFromGMT))
        return finalDate
    }
    
    static func setLoaderStyle() {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor(UIColor(white: 0, alpha: 0.8))
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setMinimumDismissTimeInterval(0.3)
    }
    
    static func loaderShow(_ text: String = "Loading...") {
        SVProgressHUD.show(withStatus: text)
    }
    
    static func loaderSuccess(_ text: String = "") {
        CommonMethods.loaderHide()
        SVProgressHUD.showSuccess(withStatus: text)
    }
    
    static func loaderError(_ text: String = "") {
        CommonMethods.loaderHide()
        SVProgressHUD.showError(withStatus: text)
    }
    
    static func loaderShowProgress(_ progress: Float, message: String) {
        SVProgressHUD.showProgress(progress, status: message)
    }
    
    static func loaderHide() {
        SVProgressHUD.dismiss()
    }
    
    static func showActivityIndicator(_ status: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = status
    }
    
    static func addShadow(_ layer: CALayer, color: UIColor = UIColor.black) {
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowOpacity = 1
        layer.shadowRadius = 1
        layer.shadowColor = color.cgColor
    }
    
    static func animateCell(_ cell: UICollectionViewCell) {
        cell.transform = CGAffineTransform.identity.scaledBy(x: 0.3, y: 0.3)
        cell.alpha = 0
        
        UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
            cell.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            cell.alpha = 1
            
        }, completion: nil
        )
    }
    
    static func makeNavigationBarToTransparent(navigationBar: UINavigationBar?) {
        navigationBar?.setBackgroundImage(UIImage(), for: .default)
        navigationBar?.shadowImage = UIImage()
        navigationBar?.isTranslucent = true
    }
    
    static func dismakeNavigationBarToTransparent(navigationBar: UINavigationBar?) {
        navigationBar?.setBackgroundImage(nil, for: .default)
        navigationBar?.shadowImage = nil
        navigationBar?.isTranslucent = false
    }
    
    static func imageWithColor(_ color: UIColor, size: CGSize = CGSize(width: 60, height: 60)) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        return image!;
    }
    
    //MARK: Clear cache after every 15 minutes
    
    /*fileprivate static var cacheTimer: DispatchSourceTimer?
    
    static func clearSDWebImageCache(_ interval: Int) {
        
        let queue = DispatchQueue(label: "", attributes: .concurrent)
        
        cacheTimer?.cancel()        // cancel previous timer if any
        
        cacheTimer = DispatchSource.makeTimerSource(queue: queue)
        
        cacheTimer?.scheduleRepeating(deadline: .now(), interval: .seconds(interval), leeway: .seconds(1))
        
        cacheTimer?.setEventHandler {
            
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
            //clear here
        }
        
        cacheTimer?.resume()
        
    }

    static func stopSDWebImgeCacheTimer() {
        if let timer = cacheTimer {
            timer.cancel()
            self.cacheTimer = nil
        }
    }*/
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let correctImage = correctlyOrientedImage(image)
        
        let scale = newWidth / correctImage.size.width
        let newHeight = correctImage.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        correctImage.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static func correctlyOrientedImage(_ image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    static func printJSON(dictionary dic: [String: Any]) {
        let theJSONData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.utf8.rawValue)
        print(theJSONText!)
    }
    
    static func printJSONFromData(data: Data) {
        let theJSON = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
        print(theJSON)
    }

}
