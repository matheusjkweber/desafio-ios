//
//  ShotCell.swift
//  DesafioUol
//
//  Created by matheus weber on 10/2/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import UIKit

class ShotCell: UITableViewCell{
    @IBOutlet weak var shotTitleLabel: UILabel!
    @IBOutlet weak var shotCreatedLabel: UILabel!
    @IBOutlet weak var shotImageView: UIImageView!
    
    func setup(shot: ShotModel){
        shotTitleLabel.text = shot.title
        shotCreatedLabel.text = shot.getFormattedDate()
        
        
    }
}
