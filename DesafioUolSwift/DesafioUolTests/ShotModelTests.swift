//
//  ShotModelTests.swift
//  DesafioUol
//
//  Created by matheus weber on 10/2/17.
//  Copyright © 2017 matheus weber. All rights reserved.
//

import XCTest
@testable import DesafioUol

class ShotModelTests: XCTestCase{
    var shot: ShotModel? = nil
    
    override func setUp() {
        super.setUp()
        
        let imageDict = ["hidpi": "https://cdn.dribbble.com/users/952958/screenshots/3844243/food-web.png",
                         "normal": "https://cdn.dribbble.com/users/952958/screenshots/3844243/food-web_1x.png",
                         "teaser": "https://cdn.dribbble.com/users/952958/screenshots/3844243/food-web_teaser.png",
                         ]
        let authorDict = ["name": "Giga Tamarashvili"]
        let dict:anyDict = ["comments_count": 30, "images": imageDict , "user": authorDict, "likes_count": 553, "views_count": 6406, "created_at": "2017-10-01T18:20:13Z"]
        shot = ShotModel(with: dict)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitWithJson(){
        XCTAssertTrue(shot?.comments_count == 30)
        XCTAssertTrue(shot?.likes_count == 553)
        XCTAssertTrue(shot?.views_count == 6406)
        XCTAssertTrue(shot?.image?.teaser == "https://cdn.dribbble.com/users/952958/screenshots/3844243/food-web_teaser.png")
        XCTAssertTrue(shot?.image?.normal == "https://cdn.dribbble.com/users/952958/screenshots/3844243/food-web_1x.png")
        XCTAssertTrue(shot?.author?.name == "Giga Tamarashvili")
        XCTAssertTrue(shot?.created_at == "2017-10-01T18:20:13Z")
    }
    
    func testFormattedDate(){
        XCTAssertTrue(shot?.getFormattedDate() == "2017-10-01")
    }
    
    func testImageURL(){
        XCTAssertNotNil(URL(string: (shot?.image?.teaser)!))
    }
}
